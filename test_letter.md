---
documentclass: article
author: Super Prof
position: Distinguished Professor
department: Department of Rec Letters
address:
  - My Office Nr.
  - 900 University Ave
  - Riverside, CA 92521
recipient:
  - The Best Foundation Ever
  - "Re: Recommendation for This Rad Person"
secondary-style: false
# signature-image: 
phone:
email: sprof@ucr.edu
website: https://superprof.com
valediction: Sincerely
valediction-margin: 0.3  # fraction of pagewidth from the left edge
fontsize: 10
sansbody: false
indent: false
left-align: false  # versus justify
lang: en
colorlinks: true
graphics-directory:  # relative path to image directory
fallback-fonts:
  - DejaVu Sans
  - Noto Sans CJK JP
  - Noto Sans Myanmar
  - Noto Color Emoji
  - Noto Sans Symbols
header-includes:
  - \usepackage{lipsum}
  - \usepackage{metalogo}
---

Dear Fellowship Committee:

I recommend this person. They are rad.

Here are some extended characters in \LuaLaTeX:  မြန်မာ emanɥɛl Mācron Hawaiʻi (╯°□°)╯︵ ┻━┻ 💩

\lipsum[1-2]

# Section

This section discusses their research. If indent is enabled, no indent after section titles.

If indent is enabled, subsequent paragraphs are indented.

## Subsection

This subsection discusses their importance to the field

\lipsum[1]

# Section

This section discusses everything else

This person's "amazing." Please do not hesitate to contact me should you have questions or require further information in support of this person's application.
